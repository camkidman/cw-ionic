import {Component} from 'angular2/core';
import {APIService} from "../../services/api.service";
import {CORE_DIRECTIVES} from "angular2/common";
import {ControlGroup, Control} from "angular2/common";
import {Page} from 'ionic-angular';
import {Injectable} from "angular2/core";
import {HttpClient} from "../../services/http.service";
import {ScheduleRowComponent} from "../../components/schedule-row.component";

@Injectable()
@Page({
    selector: 'cw-schedule-form',
    templateUrl: 'build/pages/schedule-form/schedule-form.html',
    providers: [CORE_DIRECTIVES, APIService, HttpClient],
    directives: [ScheduleRowComponent]
})

export class ScheduleFormPage {
    userId:number;
    public userScheduleJSON:any;
    goals:Array<Object>;
    public daysOfTheWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    personalDetails:Array<Object>;
    workouts:Array<Object>;
    user:Object;
    scheduleControlGroup = new ControlGroup({});
    scheduleId:number;

    constructor(private _apiService:APIService, private _http:HttpClient) {
        this.userId = localStorage.getItem("user_id");
        this.getUserSchedule();
    }

    getUserSchedule() {
        return new Promise((resolve, reject) => {
            this._http.get(`${this._apiService.baseUrl}/users/${this.userId}/schedule`)
                .subscribe(
                    data => { console.log(data),
                        this.userScheduleJSON = data.json(),
                        console.log(this.userScheduleJSON),
                        this.scheduleId = this.userScheduleJSON.id},
                    err => reject(err),
                    () => console.log(this.workouts)
                );
        })
    }
    
    onSubmit(data) {
        var scheduleHash = {"schedule": {"id": this.scheduleId, "workout_windows_attributes": []}}; 
        for (var key in data) {
            var value = data[key];
            console.log(value);
            var formattedData = {};
            var em = {};
            var m = {};
            var l = {};
            var af = {};
            var eve = {};
            var night = {};
            formattedData["weekday"] = value.day;
            em["start_time"] = value.earlyMorning.start_time;
            em["end_time"] = value.earlyMorning.end_time;
            em["weekday"] = formattedData["weekday"];
            scheduleHash["schedule"]["workout_windows_attributes"].push(em);
            
            m["start_time"] = value.morning.start_time;
            m["end_time"] = value.morning.end_time;
            m["weekday"] = formattedData["weekday"];
            scheduleHash["schedule"]["workout_windows_attributes"].push(m);
            
            l["start_time"] = value.lunch.start_time;
            l["end_time"] = value.lunch.end_time;
            l["weekday"] = formattedData["weekday"];
            scheduleHash["schedule"]["workout_windows_attributes"].push(l);
            
            af["start_time"] = value.afternoon.start_time;
            af["end_time"] = value.afternoon.end_time;
            af["weekday"] = formattedData["weekday"];
            scheduleHash["schedule"]["workout_windows_attributes"].push(af);
            
            eve["start_time"] = value.evening.start_time;
            eve["end_time"] = value.evening.end_time;
            eve["weekday"] = formattedData["weekday"];
            scheduleHash["schedule"]["workout_windows_attributes"].push(eve);
            
            night["start_time"] = value.night.start_time;
            night["end_time"] = value.night.end_time;
            night["weekday"] = formattedData["weekday"];
            scheduleHash["schedule"]["workout_windows_attributes"].push(night);
        }
        console.log(scheduleHash["schedule"]["workout_windows_attributes"]);
        return new Promise((resolve, reject) => {
            let exerciseDetails = data;
            let initialTestParams = {workout: {exercise_details_attributes: []}};
            for (var item in exerciseDetails) {
                if (exerciseDetails.hasOwnProperty(item)) {
                    initialTestParams.workout.exercise_details_attributes.push(exerciseDetails[item]);
                }
            }
            this._http.patch(`${this._apiService.baseUrl}/users/${this.userId}/schedule/`, JSON.stringify(scheduleHash))
                .subscribe(
                    data => { console.log("schedule updated!")},
                    err => reject(err),
                    () => console.log("finished updating schedule!")
                );
        })
    }
}
