import {Page} from 'ionic-angular';
import {FORM_DIRECTIVES, FormBuilder, ControlGroup, Validators, AbstractControl} from 'angular2/common';
import {APIService} from '../../services/api.service';
import {HttpClient} from '../../services/http.service';
import {NavController} from "ionic-angular";
import {Response} from "angular2/http";
import {UserDashboardPage} from "../../pages/user-dashboard/user-dashboard";
import {LoginPage} from "../../pages/login/login";

@Page({
  templateUrl: 'build/pages/register/register.html',
  directives: [FORM_DIRECTIVES],
  providers: [APIService, HttpClient, LoginPage]
})
export class RegisterPage {
    registrationForm:ControlGroup;
    emailControl:AbstractControl;
    passwordControl:AbstractControl;
    passwordConfirmationControl:AbstractControl;
    apiService:APIService;
    responseData:Response;
    
    constructor(fb: FormBuilder, public api:APIService, public nav:NavController, public http:HttpClient, public loginPage:LoginPage) {
        this.registrationForm = fb.group({  
            'email': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            'passwordConfirmation': ['', Validators.compose([Validators.required, Validators.minLength(8)])] // need to make this the same as PW at some point
        });
 
        this.emailControl = this.registrationForm.controls['email'];     
        this.passwordControl = this.registrationForm.controls['password'];
        this.passwordConfirmationControl = this.registrationForm.controls['passwordConfirmation'];
    }
    
    registerAndLogin(formData) { 
        //TODO: ACTUALLY ADD A REAL AUTHENTICATION SYSTEM
        return new Promise((resolve, reject) => {
            let creds = {email: formData.email, password: formData.password, password_confirmation: formData.passwordConfirmation};
            let loginCreds = {email: formData.email, password: formData.password};
            console.log(JSON.stringify(creds));
            this.http.post(`${this.api.baseUrl}/auth`, JSON.stringify(creds))
                .subscribe(
                    data => {this.loginPage.login(loginCreds), this.nav.setRoot(UserDashboardPage)},
                    err => reject(err),
                    () => console.log("woot"));
        })
    } 
}
