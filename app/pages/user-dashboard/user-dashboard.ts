import {Component} from 'angular2/core';
import {APIService} from "../../services/api.service";
import {CORE_DIRECTIVES} from "angular2/common";
import {DashboardWorkoutComponent} from "../../components/dashboard-workout.component";
import {ControlGroup, Control} from "angular2/common";
import {Page} from 'ionic-angular';
import {forwardRef} from 'angular2/core';
import {Injectable} from "angular2/core";
import {HttpClient} from "../../services/http.service";

@Injectable()
@Page({
    selector: 'cw-user-dashboard',
    templateUrl: 'build/pages/user-dashboard/user-dashboard.html',
    inputs: ['userDashboardJSON', 'goals', 'user'],
    providers: [CORE_DIRECTIVES, APIService, HttpClient],
    directives: [DashboardWorkoutComponent]
})

export class UserDashboardPage {
    userId:number;
    public userDashboardJSON:any;
    goals:Array<Object>;
    personalDetails:Array<Object>;
    workouts:Array<Object>;
    user:Object;
    workoutControlGroup = new ControlGroup({});

    constructor(private _apiService:APIService, private _http:HttpClient) {
        this.userId = localStorage.getItem("user_id");
        console.log("user constructed");
        this.getUserDashboardData();
    }

    getUserDashboardData() {
        return new Promise((resolve, reject) => {
            this._http.get(`${this._apiService.baseUrl}/users/${this.userId}/dashboard`)
                .subscribe(
                    data => { console.log(data),
                        this.userDashboardJSON = data.json(),
                        this.goals = this.userDashboardJSON.goals,
                        this.personalDetails = this.userDashboardJSON.personal_details,
                        this.workouts = this.userDashboardJSON.workouts,
                        this.user = this.userDashboardJSON,
                        console.log(this.userDashboardJSON)},
                    err => reject(err),
                    () => console.log(this.workouts)
                );
        })
    }
}
