import {Page} from 'ionic-angular';
import {FORM_DIRECTIVES, FormBuilder, ControlGroup, Validators, AbstractControl} from 'angular2/common';
import {APIService} from '../../services/api.service';
import {HttpClient} from '../../services/http.service';
import {NavController} from "ionic-angular";
import {Response} from "angular2/http";
import {UserDashboardPage} from "../../pages/user-dashboard/user-dashboard";

@Page({
  templateUrl: 'build/pages/login/login.html',
  directives: [FORM_DIRECTIVES],
  providers: [APIService, HttpClient]
})
export class LoginPage {
    authForm:ControlGroup;
    emailControl:AbstractControl;
    passwordControl:AbstractControl;
    apiService:APIService;
    email:string;
    password:string;
    responseData:Response;
    jsonResponseBody:any;
    
    constructor(fb: FormBuilder, public api:APIService, public nav:NavController, public http:HttpClient) {
        this.authForm = fb.group({  
            'email': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
        });
 
        this.emailControl = this.authForm.controls['email'];     
        this.passwordControl = this.authForm.controls['password'];
    }
    
    login(formData) { 
        // console.log(data);
        //  //TODO: ACTUALLY ADD A REAL AUTHENTICATION SYSTEM
        return new Promise((resolve, reject) => {
            let creds = {email: formData.email, password: formData.password};
            this.http.post(`${this.api.baseUrl}/auth/sign_in`, JSON.stringify(creds))
                .subscribe(
                    data => { this.responseData = data,
                        localStorage.setItem("Client", this.responseData.headers.get("Client")),
                        localStorage.setItem("Access-Token", this.responseData.headers.get("Access-Token")),
                        this.jsonResponseBody = this.responseData.json(),
                        localStorage.setItem("user_id", this.jsonResponseBody.data.id)},
                    err => reject(err),
                () => console.log(this.nav.setRoot(UserDashboardPage))
            );
        })
    } 
}
