import {Injectable} from "angular2/core";
import {Response} from "angular2/http";
import 'rxjs/add/operator/map';
import {Headers} from "angular2/http";
import {NavController} from "ionic-angular";

@Injectable()
export class APIService {
    baseUrl:string;
    email:string;
    password:string;
    passwordConfirmation:string;
    responseData:Response;
    jsonResponseBody:any;
    nav:NavController;

    constructor() {
        this.baseUrl = "http://localhost:3009";
    }

    // logout() {
    //     return new Promise((resolve, reject) => {
    //         this.http.delete(`${this.baseUrl}/auth/sign_out`)
    //             .subscribe(
    //                 data => { console.log("signed out!"),
    //                 localStorage.removeItem("Client"),
    //                 localStorage.removeItem("Access-Token")},
    //                 err => reject(err),
    //                 () => console.log("finished signing out")
    //             );
    //     })
    // }
}