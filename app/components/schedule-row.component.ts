import {Component, Input} from 'angular2/core';
import {NgForm} from 'angular2/common';
import {ControlGroup, Control} from "angular2/common";
import {Injectable} from "angular2/core";
// gah
@Injectable()
@Component({
    selector: 'schedule-row',
    template: `
    <div>
        <ion-col width-10><input type="text" class="form-control" disabled [ngFormControl]="dayControl" ([ngModel])="day"></ion-col>
        <ion-col width-10>7 or Before<input type="checkbox" class="form-control" [ngFormControl]="earlyMorningControl" ([ngModel])="earlyMorning" /></ion-col>
        <ion-col width-10>Between 7-11<input type="checkbox" class="form-control" [ngFormControl]="morningControl" ([ngModel])="morning" /></ion-col>
        <ion-col width-10>Between 11-2<input type="checkbox" class="form-control" [ngFormControl]="lunchControl" ([ngModel])="lunch" /></ion-col>
        <ion-col width-10>Between 3-5<input type="checkbox" class="form-control" [ngFormControl]="afternoonControl" ([ngModel])="afternoon" /></ion-col>
        <ion-col width-10>Between 5-9<input type="checkbox" class="form-control" [ngFormControl]="eveningControl" ([ngModel])="evening" /></ion-col>
        <ion-col width-10>After 10<input type="checkbox" class="form-control" [ngFormControl]="nightControl" ([ngModel])="night" /></ion-col>
    </div>
    `,
    directives: []
})

export class ScheduleRowComponent {
    dayControl = new Control();
    earlyMorningControl = new Control();
    morningControl = new Control();
    lunchControl = new Control();
    afternoonControl = new Control();
    eveningControl = new Control();
    nightControl = new Control();
    timeControlGroup = new ControlGroup({});

    @Input() weekday;
    @Input() scheduleControlGroup;
    ngOnInit() {
        this.timeControlGroup.addControl("day", this.dayControl);
        console.log(this.weekday);
        this.dayControl.updateValue(this.weekday);
        
        this.timeControlGroup.addControl("earlyMorning", this.earlyMorningControl);
        this.earlyMorningControl.updateValue({"start_time":"0500","end_time":"0700"});
        
        this.timeControlGroup.addControl("morning", this.morningControl);
        this.morningControl.updateValue({"start_time":"0700","end_time":"1100"});
        
        this.timeControlGroup.addControl("lunch", this.lunchControl);
        this.lunchControl.updateValue({"start_time":"1100","end_time":"1400"});
        
        this.timeControlGroup.addControl("afternoon", this.afternoonControl);
        this.afternoonControl.updateValue({"start_time":"1500","end_time":"1700"});
        
        this.timeControlGroup.addControl("evening", this.eveningControl);
        this.eveningControl.updateValue({"start_time":"1700","end_time":"2100"});
        
        this.timeControlGroup.addControl("night", this.nightControl);
        this.nightControl.updateValue({"start_time":"2100","end_time":"2359"});
        
        this.scheduleControlGroup.addControl(this.weekday, this.timeControlGroup);
    }

}